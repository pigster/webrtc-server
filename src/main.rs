#![recursion_limit="256"]
extern crate clap;
extern crate gstreamer as gst;
extern crate gstreamer_sdp as gst_sdp;
extern crate gstreamer_webrtc as gst_webrtc;
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
extern crate whoami;
extern crate futures;

mod phoenix_channels;
use phoenix_channels::PhoenixClient;

use async_std::prelude::*;
use async_std::task;

use futures::{select, FutureExt};
use futures::channel::mpsc::{UnboundedSender, UnboundedReceiver};

use futures::executor::block_on;

use gst::prelude::*;
use std::sync::{Arc, Mutex, Weak};
use std::collections::HashMap;
use std::time::Duration;

struct ChannelMessage {
  channel: String,
  payload: serde_json::Value
}

struct AppState {
  pipeline: gst::Pipeline, 
  phx_client: Option<PhoenixClient>,
  msg_transmitter: Arc<Mutex<UnboundedSender<ChannelMessage>>>,
  client_name: String,
  channel_name: String
}

impl AppState {
  pub fn prepare_for_move(&self) -> (gst::glib::WeakRef<gst::Pipeline>, Weak<Mutex<UnboundedSender<ChannelMessage>>>, String, String) {
    return (self.pipeline.downgrade(), Arc::downgrade(&self.msg_transmitter), self.client_name.clone(), self.channel_name.clone());
  }

  pub fn ingest((pipeline, msg_transmitter, client_name, channel_name): (gst::glib::WeakRef<gst::Pipeline>, Weak<Mutex<UnboundedSender<ChannelMessage>>>, String, String)) -> AppState {
    return AppState::ingest_ref(&(pipeline, msg_transmitter, client_name, channel_name));
  }

  pub fn ingest_ref((pipeline, msg_transmitter, client_name, channel_name): &(gst::glib::WeakRef<gst::Pipeline>, Weak<Mutex<UnboundedSender<ChannelMessage>>>, String, String)) -> AppState {
    return AppState {
      pipeline: pipeline.upgrade().expect("Failed to upgrade pipeline ref."),
      phx_client: None,
      msg_transmitter: msg_transmitter.upgrade().expect("Failed to upgrade msg_transmitter ref."),
      client_name: client_name.clone(),
      channel_name: channel_name.clone()
    }
  }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
enum SdpIceMsg {
  Ice {
    candidate: String,
    #[serde(rename = "sdpMLineIndex")]
    sdp_mline_index: u32,
  },
  Sdp {
    #[serde(rename = "type")]
    type_: String,
    sdp: String,
  },
}

#[derive(Serialize, Deserialize)]
struct PeerMessage {
  from: String,
  to: String,
  msg: SdpIceMsg,
}

#[derive(Debug, Serialize, Deserialize)]
struct PresenceDiffMessage {
  joins: HashMap<String, serde_json::Value>,
  leaves: HashMap<String, serde_json::Value>
}

#[derive(Debug, Serialize, Deserialize)]
struct ResponseMessage {
  presence: HashMap<String, serde_json::Value>
}

#[derive(Debug, Serialize, Deserialize)]
struct ReplyMessage {
  response: ResponseMessage,
  status: String
}

fn on_incoming_decodebin_stream(pad: &gst::Pad, peer: &String, app_state: AppState) {
  let caps = pad.get_current_caps().unwrap();
  let name = caps.get_structure(0).unwrap().get_name();

  if !name.starts_with("audio/") {
    println!("Unknown pad {:?}, ignoring", pad);
    return;
  }

  let outbin_o = app_state.pipeline.get_by_name(&format!("outbin-{}", peer));
  if outbin_o.is_none() { return; }

  let outbin = outbin_o.unwrap().downcast::<gst::Bin>().unwrap();

  let q = gst::ElementFactory::make("queue", None).unwrap();
  let conv = gst::ElementFactory::make("audioconvert", None).unwrap();
  let resample = gst::ElementFactory::make("audioresample", None).unwrap();

  outbin.add_many(&[&q, &conv, &resample]).unwrap();
  gst::Element::link_many(&[&q, &conv, &resample]).unwrap();

  resample.sync_state_with_parent().unwrap();
  q.sync_state_with_parent().unwrap();
  conv.sync_state_with_parent().unwrap();

  let qpad = q.get_static_pad("sink").unwrap();
  pad.link(&qpad).unwrap();

  let srcpad = resample.get_static_pad("src").unwrap();
  let ghost_src = gst::GhostPad::new(Some("src"), &srcpad).unwrap();
  ghost_src.set_active(true).unwrap();
  outbin.add_pad(&ghost_src).unwrap();

  let srcpad_ob = outbin.get_static_pad("src").unwrap();

  let mixer = app_state.pipeline.get_by_name("audiomix").unwrap();
  let mixerpad = mixer.get_request_pad("sink_%u").unwrap();
  srcpad_ob.link(&mixerpad).unwrap();
}

fn on_incoming_stream(webrtc: gst::Element, outpad: gst::Pad, app_state: AppState) {
  let peer = String::from(webrtc.get_name());
  let outbin = gst::Bin::new(Some(&format!("outbin-{}", peer)));
  app_state.pipeline.add(&outbin).unwrap();
  outbin.sync_state_with_parent().unwrap();
  
  let decodebin = gst::ElementFactory::make("decodebin", None).unwrap();
 
  let app_state_d = app_state.prepare_for_move();
  decodebin.connect_pad_added(move |_decodebin, pad| {
    let app_state = AppState::ingest_ref(&app_state_d);
    on_incoming_decodebin_stream(pad, &peer, app_state);
  });

  outbin.add(&decodebin).unwrap();
  decodebin.sync_state_with_parent().unwrap();

  let sinkpad = decodebin.get_static_pad("sink").unwrap();
  let ghost_sink = gst::GhostPad::new(Some("sink"), &sinkpad).unwrap();
  ghost_sink.set_active(true).unwrap();
  outbin.add_pad(&ghost_sink).unwrap();

  let sinkpad_ob = outbin.get_static_pad("sink").unwrap();
  outpad.link(&sinkpad_ob).unwrap();
}

fn on_negotiation_needed(webrtc: gst::Element, app_state: AppState) {
  let app_state_d = app_state.prepare_for_move();
  let peer = String::from(webrtc.get_name());
  let promise = gst::Promise::new_with_change_func(move |promise| {
    let app_state = AppState::ingest(app_state_d);
    let reply = match promise.wait() {
      gst::PromiseResult::Replied => promise.get_reply().unwrap(),
      err => panic!("Offer creation future got no reponse: {:?}", err)
    };

    let offer = reply
        .get_value("offer")
        .unwrap()
        .get::<gst_webrtc::WebRTCSessionDescription>()
        .expect("Invalid argument");

    match app_state.pipeline.get_by_name(&peer) {
      Some(webrtc) => { webrtc.emit("set-local-description", &[&offer, &None::<gst::Promise>]).unwrap(); },
      None => return
    };   

    let sdp_offer = offer.get_sdp().as_text().unwrap();
    println!("SDP OFFER to {:}\n{:}", peer, sdp_offer);
    
    let msg = PeerMessage {
      from: app_state.client_name.clone(),
      to: peer,
      msg: SdpIceMsg::Sdp {
        type_: String::from("offer"),
        sdp: sdp_offer
      }
    };

    let ch_msg = ChannelMessage {
      channel: app_state.channel_name.clone(),
      payload: serde_json::to_value(msg).unwrap()
    };
    
    let sender = app_state.msg_transmitter.lock().unwrap();
    sender.unbounded_send(ch_msg).unwrap();
  });

  webrtc.emit("create-offer", &[&None::<gst::Structure>, &promise]).unwrap();
}

fn ice_created(webrtc: gst::Element, candidate: String, sdp_mline_index: u32, app_state: AppState) {
  let peer = String::from(webrtc.get_name());
  let msg = PeerMessage {
    from: app_state.client_name.clone(),
    to: peer,
    msg: SdpIceMsg::Ice {
      candidate: candidate,
      sdp_mline_index: sdp_mline_index,
    }
  };

  let ch_msg = ChannelMessage {
    channel: app_state.channel_name.clone(),
    payload: serde_json::to_value(msg).unwrap()
  };

  let sender = app_state.msg_transmitter.lock().unwrap();
  sender.unbounded_send(ch_msg).unwrap();
}

fn create_webrtc_pipeline(offer: bool, from: &String, app_state: &AppState) -> gst::Element {
  let q = gst::ElementFactory::make("queue", Some(&format!("queue-{}", from))).unwrap();
  let webrtc = gst::ElementFactory::make("webrtcbin", Some(&from)).unwrap();

  app_state.pipeline.add_many(&[&webrtc, &q]).unwrap();
  let q_srcpad = q.get_static_pad("src").unwrap();
  let rtc_sinkpad = webrtc.get_request_pad("sink_%u").unwrap();
  q_srcpad.link(&rtc_sinkpad).unwrap();

  let a_tee = app_state.pipeline.get_by_name("audiotee").unwrap();
  let a_tee_src_pad = a_tee.get_request_pad("src_%u").unwrap();
  let q_sinkpad = q.get_static_pad("sink").unwrap();
  a_tee_src_pad.link(&q_sinkpad).unwrap();

  if offer {
    let app_state_d = app_state.prepare_for_move();
    webrtc.connect("on-negotiation-needed", false, move |values| {
      let app_state = AppState::ingest_ref(&app_state_d);
      let webrtc = values[0].get::<gst::Element>().expect("Invalid argument");
      on_negotiation_needed(webrtc, app_state);
      None
    }).unwrap();  
  }
  
  let app_state_d = app_state.prepare_for_move();
  webrtc.connect("on-ice-candidate", false, move |values| {
    let app_state = AppState::ingest_ref(&app_state_d);
    let webrtc = values[0].get::<gst::Element>().expect("Invalid argument");
    let mlineindex = values[1].get::<u32>().expect("Invalid argument");
    let candidate = values[2].get::<String>().expect("Invalid argument");
    ice_created(webrtc, candidate, mlineindex, app_state);
    None
  }).unwrap();  

  let app_state_d = app_state.prepare_for_move();
  webrtc.connect("pad-added", false, move |values| {
    let app_state = AppState::ingest_ref(&app_state_d);
    let webrtc = values[0].get::<gst::Element>().expect("Invalid argument");
    let outpad = values[1].get::<gst::Pad>().expect("Invalid argument");  
    on_incoming_stream(webrtc, outpad, app_state);
    None
  }).unwrap();

  q.sync_state_with_parent().unwrap();
  webrtc.sync_state_with_parent().unwrap();

  return webrtc;
}

fn cleanup_peer_leftovers_from_pipeline(peer: &String, app_state: &AppState) {
  let webrtc_o = app_state.pipeline.get_by_name(&peer);
  let outbin_o = app_state.pipeline.get_by_name(&format!("outbin-{}", peer));

  if outbin_o.is_some() {
    let outbin = outbin_o.unwrap();
    let audiomix = app_state.pipeline.get_by_name("audiomix").unwrap();
    
    let srcpad = outbin.get_static_pad("src").unwrap();    
    let mix_sinkpad = srcpad.get_peer().unwrap();
    
    srcpad.unlink(&mix_sinkpad).unwrap();
    audiomix.release_request_pad(&mix_sinkpad);

    app_state.pipeline.remove(&outbin).unwrap();
    outbin.set_state(gst::State::Null).unwrap();
  }  

  if webrtc_o.is_some() {
    let webrtc = webrtc_o.unwrap();
    app_state.pipeline.remove(&webrtc).unwrap();
    webrtc.set_state(gst::State::Null).unwrap();
  }  
}

fn remove_peer_from_pipeline(peer: &String, app_state: &AppState) {
  let queue_o = app_state.pipeline.get_by_name(&format!("queue-{}", peer));
  
  if queue_o.is_some() {
    let queue = queue_o.unwrap();
    let sinkpad = queue.get_static_pad("sink").unwrap();
    let at_srcpad = sinkpad.get_peer().unwrap();

    let app_state_d = app_state.prepare_for_move();
    let peer_copy = String::from(peer);
    at_srcpad.add_probe(gst::PadProbeType::BLOCK, move |_, _| {
      let app_state = AppState::ingest_ref(&app_state_d);

      let queue_o = app_state.pipeline.get_by_name(&format!("queue-{}", peer_copy));
      if queue_o.is_some() {
        let queue = queue_o.unwrap();
        let sinkpad = queue.get_static_pad("sink").unwrap();
        let at_srcpad = sinkpad.get_peer().unwrap();
      
        at_srcpad.unlink(&sinkpad).unwrap();

        let audiotee = app_state.pipeline.get_by_name("audiotee").unwrap();
        audiotee.release_request_pad(&at_srcpad);
    
        app_state.pipeline.remove(&queue).unwrap();
        queue.set_state(gst::State::Null).unwrap();
      }

      cleanup_peer_leftovers_from_pipeline(&peer_copy, &app_state);

      return gst::PadProbeReturn::Remove;
    });    
  } else {
    cleanup_peer_leftovers_from_pipeline(peer, app_state)
  }
}

fn handle_sdp(from: String, type_: String, sdp: String, app_state: &mut AppState) {
  match type_.as_str() {
    "offer" => handle_sdp_offer(from, sdp, app_state),
    "answer" => handle_sdp_answer(from, sdp, app_state),
    _ => ()
  }
}

fn handle_sdp_offer(from: String, sdp: String, app_state: &mut AppState) {
  println!("SDP OFFER from {:}\n{:}", from, sdp);

  // Clenup
  remove_peer_from_pipeline(&from, &app_state);

  let webrtc = create_webrtc_pipeline(false, &from, &app_state);

  let sdp_msg = gst_sdp::SDPMessage::parse_buffer(sdp.as_bytes()).expect("Failed to parse SDP offer");   
  let offer = gst_webrtc::WebRTCSessionDescription::new(gst_webrtc::WebRTCSDPType::Offer,sdp_msg);
  webrtc.emit("set-remote-description", &[&offer, &None::<gst::Promise>]).unwrap();

  let app_state_d = app_state.prepare_for_move();
  let promise = gst::Promise::new_with_change_func(move |reply| {
    let app_state = AppState::ingest_ref(&app_state_d);
    let reply = match reply.wait() {
      gst::PromiseResult::Replied => reply.get_reply().unwrap(),
      _ => panic!("Answer not created")
    };

    let answer = reply
      .get_value("answer")
      .unwrap()
      .get::<gst_webrtc::WebRTCSessionDescription>()
      .expect("Invalid argument");
      
    match app_state.pipeline.get_by_name(&from) {
      Some(webrtc) => { webrtc.emit("set-local-description", &[&answer, &None::<gst::Promise>]).unwrap(); },
      None => return
    };   
    
    let sdp_answer = answer.get_sdp().as_text().unwrap();
    
    println!("SDP ANSWER to {:}\n{:}", from, sdp_answer);
    
    let msg = PeerMessage {
      from: app_state.client_name.clone(),
      to: from,
      msg: SdpIceMsg::Sdp {
        type_: String::from("answer"),
        sdp: sdp_answer
      }
    };

    let ch_msg = ChannelMessage {
      channel: app_state.channel_name.clone(),
      payload: serde_json::to_value(msg).unwrap()
    };    

    let sender = app_state.msg_transmitter.lock().unwrap();
    sender.unbounded_send(ch_msg).unwrap();
  });  

  webrtc.emit("create-answer", &[&None::<gst::Structure>, &promise]).unwrap();  
}

fn handle_sdp_answer(from: String, sdp: String, app_state: &AppState) {
  println!("SDP ANSWER from {:}\n{:}", from, sdp);

  let ret = gst_sdp::SDPMessage::parse_buffer(sdp.as_bytes()).expect("Failed to parse SDP answer");
  let answer = gst_webrtc::WebRTCSessionDescription::new(gst_webrtc::WebRTCSDPType::Answer, ret);

  match app_state.pipeline.get_by_name(&from) {
    Some(webrtc) => { webrtc.emit("set-remote-description", &[&answer, &None::<gst::Promise>]).unwrap(); },
    None => ()
  };
}

fn handle_ice(from: String, sdp_mline_index: u32, candidate: String, app_state: &AppState) {
  // println!("ICE {:} {:},{:}", from, sdp_mline_index, candidate);

  match app_state.pipeline.get_by_name(&from) {
    Some(webrtc) => { webrtc.emit("add-ice-candidate", &[&sdp_mline_index, &candidate]).unwrap(); },
    None => ()
  };   
}

fn process_join_reply(payload: serde_json::Value, app_state: &AppState) {
  //println!("REPLY {:?}", payload);

  let response_r: Result<ReplyMessage, serde_json::Error> = serde_json::from_value(payload);  
  match response_r {
    Ok(reply) => {
      for (k, _v) in reply.response.presence.iter() {
        create_webrtc_pipeline(true,k,&app_state);
      }
    }
    _ => ()
  }
}

fn process_incoming_peer_msg(payload: serde_json::Value, app_state: &mut AppState) {
  // println!("MSG {:?}", payload);

  let json_msg_r: Result<PeerMessage, serde_json::Error> = serde_json::from_value(payload);
  match json_msg_r {
    Ok(json_msg) => {
      if json_msg.to == *app_state.client_name {
        match json_msg.msg {
          SdpIceMsg::Sdp { type_, sdp } => handle_sdp(json_msg.from, type_, sdp, app_state),
          SdpIceMsg::Ice { sdp_mline_index, candidate } => handle_ice(json_msg.from, sdp_mline_index, candidate, app_state)
        }
      }
    },
    _ => ()
  }
}

fn process_incoming_presence_diff(payload: serde_json::Value, app_state: &AppState) {
  //println!("PRESENCE {:?}", payload);

  let presence_r: Result<PresenceDiffMessage, serde_json::Error> = serde_json::from_value(payload);
  match presence_r {
    Ok(presence) => {
      for (k, _v) in presence.leaves.iter() {
        println!("Peer {:} left.", k);
        remove_peer_from_pipeline(&k, app_state);
      }      
    }
    _ => ()    
  }
}

async fn process_incoming_message(message: phoenix_channels::PhoenixMessage, app_state: &mut AppState) {  
  if message.topic == app_state.channel_name {
    match message.event {
      phoenix_channels::EventKind::PeerMsg => process_incoming_peer_msg(message.payload, app_state),
      phoenix_channels::EventKind::PresenceDiff => process_incoming_presence_diff(message.payload, app_state),
      _ => ()
    }
  }
}

async fn process_channel_messages(mut ws_messages: UnboundedReceiver<phoenix_channels::PhoenixMessage>, mut json_messages: UnboundedReceiver<ChannelMessage>, mut app_state: AppState) {
  loop {
    select! {
      line = ws_messages.next().fuse() => {
        let msg = line.unwrap();
        process_incoming_message(msg, &mut app_state).await;
      },
      line = json_messages.next().fuse() => {
        let msg = line.unwrap();
        app_state.phx_client.as_mut().unwrap().send(&msg.channel, msg.payload).await;
      }
    }
  }
}

async fn debug_pipeline(pipeline: gst::Pipeline) {
  loop {
    gst::debug_bin_to_dot_file_with_ts(
      &pipeline,
      gst::DebugGraphDetails::all(),
      "pipeline",
    );
    task::sleep(Duration::from_secs(10)).await;
  }
}

fn parse_args() -> (String, String, String, String) {
  let matches = clap::App::new("Intercom Server")
      .arg(
        clap::Arg::with_name("server")
            .help("Signalling server to connect to")
            .long("server")
            .required(false)
            .takes_value(true),
      )
      .arg(
        clap::Arg::with_name("room")
            .help("Room to join")
            .long("room")
            .required(false)
            .takes_value(true),
      )      
      .arg(
        clap::Arg::with_name("audiosrc")
            .help("Gstreamer audio source")
            .long("audiosrc")
            .required(false)
            .takes_value(true),
      )
      .arg(
        clap::Arg::with_name("audiosink")
            .help("Gstreamer audio sink")
            .long("audiosink")
            .required(false)
            .takes_value(true),
      )      
      .get_matches();

  let server = matches.value_of("server")
      .unwrap_or("ws://127.0.0.1:4000/socket");

  let room = matches.value_of("room")
      .unwrap_or("webrtc:intercom");
  
  let audio_src = matches.value_of("audiosrc")
      .unwrap_or("autoaudiosrc");
  
  let audio_sink = matches.value_of("audiosink")
      .unwrap_or("autoaudiosink");

  (server.to_string(), room.to_string(), audio_src.to_string(), audio_sink.to_string())
}

fn check_plugins() {
  let needed = [
    "opus", 
    "nice", 
    "webrtc", 
    "dtls", 
    "srtp",
    "rtpmanager"
  ];

  let registry = gst::Registry::get();
  let missing = needed
      .iter()
      .filter(|n| registry.find_plugin(n).is_none())
      .cloned()
      .collect::<Vec<_>>();

  if !missing.is_empty() {
    panic!("Missing plugins {:?}", missing)
  }
}

fn main() {
  // Terminate on thread panic
  let orig_hook = std::panic::take_hook();
  std::panic::set_hook(Box::new(move |panic_info| {
    orig_hook(panic_info);
    std::process::exit(1);
  }));

  gst::init().unwrap();
  check_plugins();

  let (server, room, audio_src, audio_sink) = parse_args();
  let pipeline_description = format!("audiomixer name=audiomix ! {} \
                                      tee name=audiotee ! queue ! fakesink \
                                      {} ! audio/x-raw,channels=1 ! audioconvert ! audioresample ! queue ! opusenc ! rtpopuspay ! \
                                      queue ! application/x-rtp,media=audio,encoding-name=OPUS,payload=111 ! audiotee.", audio_sink, audio_src);

  let pipeline = gst::parse_launch(&pipeline_description)
                .expect("Failed to create pipeline")
                .downcast::<gst::Pipeline>()
                .unwrap();
  let bus = pipeline.get_bus().unwrap();  

  pipeline
  .set_state(gst::State::Playing)
  .expect("Unable to set the pipeline to the `Playing` state");

  println!("Connecting to server {}", server);

  let (mut phx_client, receiver) = phoenix_channels::connect(&server);
  let client_name = format!("{}-{}", whoami::username(), std::process::id());
  
  println!("Connected");

  let join_reply = block_on(phx_client.join(&room, &client_name));

  let (json_message_tx, json_message_rx) = futures::channel::mpsc::unbounded::<ChannelMessage>();

  let app_state = AppState {
    pipeline: pipeline.clone(),
    phx_client: Some(phx_client),    
    msg_transmitter: Arc::new(Mutex::new(json_message_tx)),
    client_name: client_name,
    channel_name: room
  };

  process_join_reply(join_reply.payload, &app_state);
  task::spawn(debug_pipeline(pipeline.clone()));  
  task::spawn(process_channel_messages(receiver, json_message_rx, app_state));

  // Main GST loop
  for msg in bus.iter_timed(gst::CLOCK_TIME_NONE) {
    use gst::MessageView;
    match msg.view() {
      MessageView::StateChanged(state) => if state.get_current() == gst::State::Null { 
        println!("Pipeline terminated.");
        break; 
      },
      MessageView::Eos(..) => break,
      MessageView::Error(err) => {
        println!(
          "Error from {:?}: {} ({:?})",
          err.get_src().map(|s| s.get_path_string()),
          err.get_error(),
          err.get_debug()
        );
        break;
      }
      _ => (),
    }
  }

  pipeline
    .set_state(gst::State::Null)
    .expect("Unable to set the pipeline to the `Null` state");  
}