extern crate async_std;
extern crate futures;
extern crate url;

extern crate tungstenite;
extern crate async_tungstenite;
extern crate async_tls;

use std::fmt;
use std::time::Duration;

use serde::de::{Deserialize, Deserializer, Visitor, Unexpected};
use serde::ser::{Serialize, Serializer};

use self::async_std::prelude::*;
use self::async_std::task;

use async_std::net::TcpStream;

use self::futures::{select, FutureExt, SinkExt};
use self::futures::executor::block_on;

use self::futures::channel::mpsc::unbounded;
use self::futures::channel::mpsc::{ UnboundedSender, UnboundedReceiver};

use self::async_std::future::timeout;

use self::tungstenite::{handshake::client::Request, handshake::client::Response, Error};
use self::async_tungstenite::{client_async_with_config, WebSocketStream};

const PHOENIX_VERSION: &str = "2.0.0";
const REPLY_TIMEOUT: u64 = 5;

#[derive(Debug, Serialize, Deserialize)]
pub struct PhoenixMessage {
  pub join_ref: Option<String>,
  pub message_ref: Option<String>,
  pub topic: String,
  pub event: EventKind,
  pub payload: serde_json::Value,
}

#[derive(Debug, PartialEq)]
pub enum EventKind {
  Close,
  Error,
  Join,
  Leave,
  Reply,
  PresenceDiff,
  PeerMsg
}

pub struct PhoenixClient {
  sender: UnboundedSender<String>,
  replies: UnboundedReceiver<PhoenixMessage>,
  join_ref: u32,
  message_ref: u32  
}

impl<'de> Deserialize<'de> for EventKind {
  fn deserialize<D>(deserializer: D) -> Result<EventKind, D::Error>
  where D: Deserializer<'de>
  {
    struct FieldVisitor;

    impl<'de> Visitor<'de> for FieldVisitor {
      type Value = EventKind;

      fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "a string with a value of [phx_close, phx_error, phx_join, phx_leave, phx_reply]")
      }

      fn visit_str<E>(self, value: &str) -> Result<EventKind, E>
      where E: serde::de::Error
      {
        match value {
          "phx_close" => Ok(EventKind::Close),
          "phx_error" => Ok(EventKind::Error),
          "phx_join" => Ok(EventKind::Join),
          "phx_leave" => Ok(EventKind::Leave),
          "phx_reply" => Ok(EventKind::Reply),
          "presence_diff" => Ok(EventKind::PresenceDiff),
          "peer_msg" => Ok(EventKind::PeerMsg),
          s => Err(E::invalid_value(Unexpected::Str(s), &self)),
        }
      }
    }

    deserializer.deserialize_str(FieldVisitor)
  }
}

impl Serialize for EventKind {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where S: Serializer
  {
    let kind = match *self {
      EventKind::Close => "phx_close",
      EventKind::Error => "phx_error",
      EventKind::Join => "phx_join",
      EventKind::Leave => "phx_leave",
      EventKind::Reply => "phx_reply",
      EventKind::PresenceDiff => "presence_diff",
      EventKind::PeerMsg => "peer_msg",
    };

    serializer.serialize_str(kind)
  }
}

impl PhoenixClient {
  pub async fn join(&mut self, channel: &str, nickname: &str) -> PhoenixMessage {    
    let phx_message = json!([self.join_ref, self.message_ref.to_string(), channel, "phx_join", {"user_id": nickname}]);

    self.sender.send(phx_message.to_string()).await.unwrap();

    let reply = timeout(Duration::from_secs(REPLY_TIMEOUT), self.replies.next()).await.expect("Join timeout").unwrap();
    if reply.message_ref.clone().unwrap().parse::<u32>().unwrap() != self.message_ref {
      panic!("Wrong message id in reply");
    }
 
    self.join_ref += 1;
    self.message_ref += 1;    
    
    return reply;
  }

  pub async fn send(&mut self, channel: &str, payload: serde_json::Value) -> PhoenixMessage {
    let phx_message = json!([None::<String>, self.message_ref.to_string(), channel, "peer_msg", payload]);

    self.sender.send(phx_message.to_string()).await.unwrap();

    let reply = timeout(Duration::from_secs(REPLY_TIMEOUT), self.replies.next()).await.expect("Message timeout").unwrap();
    if reply.message_ref.clone().unwrap().parse::<u32>().unwrap() != self.message_ref {
      panic!("Wrong message id in reply");
    }

    self.message_ref += 1;

    return reply;    
  }
}

#[inline]
fn domain(request: &Request) -> Result<String, Error> {
  match request.url.host_str() {
    Some(d) => Ok(d.to_string()),
    None => Err(Error::Url("no host name in the url".into())),
  }
}

async fn connect_async(request: Request<'_>) -> Result<(WebSocketStream<TcpStream>, Response), Error> {
  let request: Request = request.into();
  let domain = domain(&request)?;
  let port = request
      .url
      .port_or_known_default()
      .expect("Bug: port unknown");

  let stream = TcpStream::connect((domain.as_str(), port)).await.expect("Connection failed");    
  stream.set_nodelay(true).unwrap();

  client_async_with_config(request, stream, None).await
}

pub fn connect(url_str: &String) -> (
  PhoenixClient,
  UnboundedReceiver<PhoenixMessage>
) {

  let addr = format!("{}/websocket?vsn={}", url_str, PHOENIX_VERSION);
  let url = url::Url::parse(&addr).unwrap();
  
  let (message_out_tx, message_out_rx) = unbounded::<String>();
  let (message_in_tx, message_in_rx) = unbounded::<PhoenixMessage>();
  
  let (reply_in_tx, reply_in_rx) = unbounded::<PhoenixMessage>();
  let (heartbeat_in_tx, heartbeat_in_rx) = unbounded::<PhoenixMessage>();

  let (ws_stream, _) = block_on(
    timeout(Duration::from_secs(5), connect_async(Request::from(url)))
  ).expect("Timeout connecting").expect("Failed to connect");

  task::spawn(porcess_ws(ws_stream, message_out_rx, message_in_tx, heartbeat_in_tx, reply_in_tx));
  task::spawn(heartbeat(message_out_tx.clone(), heartbeat_in_rx));

  let client = PhoenixClient {
    sender: message_out_tx,
    replies: reply_in_rx,
    join_ref: 0,
    message_ref: 0
  };

  return (client, message_in_rx);
}

async fn heartbeat(
  mut message_out_tx: UnboundedSender<String>,
  mut heartbeat_in_rx: UnboundedReceiver<PhoenixMessage>
) {
  loop {
    //println!("HEARTBEAT");
    let phx_message = json!([(), "heartbeat", "phoenix", "heartbeat", {}]);
    message_out_tx.send(phx_message.to_string()).await.unwrap();
    timeout(Duration::from_secs(REPLY_TIMEOUT), heartbeat_in_rx.next()).await.expect("Heartbeat timeout");    
    task::sleep(Duration::from_secs(2)).await;  
  }
}

async fn porcess_ws(
  mut ws_stream: async_tungstenite::WebSocketStream<async_std::net::TcpStream>,
  mut message_out_rx: UnboundedReceiver<String>,
  mut message_in_tx: UnboundedSender<PhoenixMessage>,
  mut heartbeat_in_tx: UnboundedSender<PhoenixMessage>,
  mut reply_in_tx: UnboundedSender<PhoenixMessage>,
) {
  loop {
    select! {
      line = ws_stream.next().fuse() => {
        match line.unwrap().unwrap() {
          tungstenite::Message::Text(msg) => {
            let ret:Result<PhoenixMessage, serde_json::Error> = serde_json::from_str(&msg);
            // FIX: implement parse error logging
            if(ret.is_ok()) {
              let pmsg = ret.unwrap();
              //println!("{:?}", pmsg);             
              if pmsg.event == EventKind::Reply {
                if pmsg.message_ref == Some(String::from("heartbeat")) {
                  heartbeat_in_tx.send(pmsg).await.unwrap();
                } else {
                  reply_in_tx.send(pmsg).await.unwrap();
                }
              } else {
                message_in_tx.send(pmsg).await.unwrap();
              }
            }
          },
          _ => ()
        }
      },
      line = message_out_rx.next().fuse() => {
        let msg = tungstenite::Message::text(line.unwrap());
        ws_stream.send(msg).await.unwrap();
      }
    }
  }    
}
